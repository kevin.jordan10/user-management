package com.kevinjordan.usermanagement.service.impl;

import static com.kevinjordan.usermanagement.constant.ResponseConstant.FAILURE;
import static com.kevinjordan.usermanagement.constant.ResponseConstant.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ActiveProfiles;

import com.kevinjordan.usermanagement.dto.UserTypeSearchDto;
import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.page.CustomPage;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.repository.UserTypeCustomRepository;
import com.kevinjordan.usermanagement.repository.UserTypeRepository;
import com.kevinjordan.usermanagement.util.MessageUtil;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;
import com.kevinjordan.usermanagement.util.ResponseUtil;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("local")
public class UserTypeServiceTest {

	@Mock
	private UserTypeRepository userTypeRepository;

	@Mock
	private UserTypeCustomRepository userTypeCustomRepository;

	@Mock
	private MessageUtil messageUtil;

	@Mock
	private ResponseUtil responseUtil;

	@Mock
	private OrikaMapperUtil mapperUtil;

	@InjectMocks
	private UserTypeServiceImpl userTypeService;

	/**
	 * Expect the saveUserType to run successfully
	 */
	@Test
	public void saveUserTypeSuccessTest() {
		UserType userType = this.createUserType();

		when(userTypeRepository.save(userType)).thenReturn(userType);
		assertEquals(SUCCESS, userTypeService.saveUserType(userType).getBody().getStatus());
	}

	/**
	 * Expect the saveUserType to fail because the user type's name has already
	 * existed
	 */
	@Test
	public void saveUserTypeFailureTest() {
		UserType userType = this.createUserType();
		List<UserType> listOfUserTypes = new ArrayList<>();
		listOfUserTypes.add(userType);

		when(userTypeRepository.findUserTypeByName(userType.getName())).thenReturn(listOfUserTypes);
		assertEquals(FAILURE, userTypeService.saveUserType(userType).getBody().getStatus());
		verify(userTypeRepository, times(0)).save(any(UserType.class));
	}

	/**
	 * Expect the getUserTypeById to run successfully
	 * 
	 * UserType will be returned
	 */
	@Test
	public void getUserTypeByIdSuccessTest() {
		UserType userType = this.createUserType();

		when(userTypeRepository.findUserTypeById(userType.getId())).thenReturn(userType);
		assertEquals(SUCCESS, userTypeService.getUserTypeById(userType.getId()).getBody().getStatus());
	}

	/**
	 * Expect the getUserTypeById to fail because the searched UserType can not be
	 * found
	 * 
	 */
	@Test
	public void getUserTypeByIdFailureTest() {
		UserType userType = this.createUserType();

		when(userTypeRepository.findUserTypeById(userType.getId())).thenReturn(null);
		assertEquals(FAILURE, userTypeService.getUserTypeById(userType.getId()).getBody().getStatus());
	}

	/**
	 * Expect the findUserTypes to success
	 */
	@Test
	public void findUserTypessSuccessTest() {
		Pagination pagination = new Pagination(1, 5, "asc", "id");
		List<UserTypeSearchDto> userTypeDtos = new ArrayList<>();
		CustomPage<UserTypeSearchDto> output = new CustomPage<>(
				new PageImpl<>(userTypeDtos, pagination.toPageable(), userTypeDtos.size()));

		when(userTypeCustomRepository.findUserTypesByParameter(null, pagination)).thenReturn(output);
		assertEquals(SUCCESS, userTypeService.findUserTypes(null, pagination).getBody().getStatus());
	}

	/**
	 * Expect the updateUser to run successfully
	 */
	@Test
	public void updateUserTypeSuccessTest() {
		UserType userType = this.createUserType();

		when(userTypeRepository.save(userType)).thenReturn(userType);
		assertEquals(SUCCESS, userTypeService.updateUserType(userType).getBody().getStatus());
	}

	/**
	 * Expect the deleteUserType to fail because the target data is not found inside
	 * the database
	 */
	@Test
	public void deleteUserTypeFailureTest() {
		UserType userType = this.createUserType();
		when(userTypeRepository.findUserTypeById(userType.getId())).thenReturn(null);
		assertEquals(FAILURE, userTypeService.deleteUserType(userType.getId()).getBody().getStatus());
		verify(userTypeRepository, times(0)).delete(any(UserType.class));
	}

	/**
	 * Expect the updateUser to fail because the user type's name has already
	 * existed
	 */
	@Test
	public void updateUserTypeFailureTest() {
		UserType userType = this.createUserType();
		List<UserType> listOfUserTypes = new ArrayList<>();
		listOfUserTypes.add(userType);

		when(userTypeRepository.findUserTypeByName(userType.getName())).thenReturn(listOfUserTypes);
		assertEquals(FAILURE, userTypeService.updateUserType(userType).getBody().getStatus());
		verify(userTypeRepository, times(0)).save(any(UserType.class));
	}

	private UserType createUserType() {
		UserType userType = new UserType("type");
		userType.setId(1);
		return userType;
	}

}
