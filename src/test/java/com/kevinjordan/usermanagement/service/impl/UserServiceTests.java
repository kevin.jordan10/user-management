package com.kevinjordan.usermanagement.service.impl;

import static com.kevinjordan.usermanagement.constant.ResponseConstant.FAILURE;
import static com.kevinjordan.usermanagement.constant.ResponseConstant.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ActiveProfiles;

import com.kevinjordan.usermanagement.dto.UserSearchDto;
import com.kevinjordan.usermanagement.model.User;
import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.page.CustomPage;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.repository.UserCustomRepository;
import com.kevinjordan.usermanagement.repository.UserRepository;
import com.kevinjordan.usermanagement.util.MessageUtil;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;
import com.kevinjordan.usermanagement.util.ResponseUtil;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("local")
public class UserServiceTests {

	@Mock
	private UserRepository userRepository;

	@Mock
	private UserCustomRepository userCustomRepository;

	@Mock
	private MessageUtil messageUtil;

	@Mock
	private ResponseUtil responseUtil;

	@Mock
	private OrikaMapperUtil mapperUtil;

	@InjectMocks
	private UserServiceImpl userService;

	/**
	 * Expect the saveUser to run successfully
	 */
	@Test
	public void saveUserSuccessTest() {
		User user = this.createUser();

		when(userRepository.save(user)).thenReturn(user);
		userService.saveUser(user);
		verify(userRepository, times(1)).save(any(User.class));
	}

	/**
	 * Expect the saveUser to fail because the user's name has already existed
	 */
	@Test
	public void saveUserFailureTest() {
		User user = this.createUser();

		List<User> listOfUsers = new ArrayList<>();
		listOfUsers.add(user);
		when(userRepository.findUserByName(user.getName())).thenReturn(listOfUsers);
		assertEquals(FAILURE, userService.saveUser(user).getBody().getStatus());
		verify(userRepository, times(0)).save(any(User.class));
	}

	/**
	 * Expect the getUserById to success
	 */
	@Test
	public void getUserByIdSuccessTest() {
		User user = this.createUser();

		when(userRepository.findUserById(user.getId())).thenReturn(user);
		assertEquals(SUCCESS, userService.getUserById(user.getId()).getBody().getStatus());
	}

	/**
	 * Expect the getUserById to fail because the searched User can not be found
	 */
	@Test
	public void getUserByIdFailureTest() {
		User user = this.createUser();

		when(userRepository.findUserById(user.getId())).thenReturn(null);
		assertEquals(FAILURE, userService.getUserById(user.getId()).getBody().getStatus());
	}

	/**
	 * Expect the findUsers to success
	 */
	@Test
	public void findUsersSuccessTest() {
		Pagination pagination = new Pagination(1, 5, "asc", "id");
		List<UserSearchDto> userDtos = new ArrayList<>();
		CustomPage<UserSearchDto> output = new CustomPage<>(
				new PageImpl<>(userDtos, pagination.toPageable(), userDtos.size()));

		when(userCustomRepository.findUsersByParameter(null, null, null, null, pagination)).thenReturn(output);
		assertEquals(SUCCESS, userService.findUsers(null, null, null, null, pagination).getBody().getStatus());
	}

	/**
	 * Expect the deleteUser to fail because the target data is not found inside the
	 * database
	 */
	@Test
	public void deleteUserFailureTest() {
		User user = this.createUser();

		when(userRepository.findUserById(user.getId())).thenReturn(null);

		assertEquals(FAILURE, userService.deleteUser(user.getId()).getBody().getStatus());
		verify(userRepository, times(0)).delete(any(User.class));
	}

	/**
	 * Expect the updateUser to run successfully
	 */
	@Test
	public void updateUserSuccessTest() {
		User user = this.createUser();

		when(userRepository.save(user)).thenReturn(user);
		assertEquals(SUCCESS, userService.updateUser(user).getBody().getStatus());
	}

	/**
	 * Expect the updateUser to fail because the user's name has already existed
	 */
	@Test
	public void updateUserFailureTest() {
		User user = this.createUser();

		List<User> listOfUsers = new ArrayList<>();
		listOfUsers.add(user);
		when(userRepository.findUserByName(user.getName())).thenReturn(listOfUsers);
		assertEquals(FAILURE, userService.updateUser(user).getBody().getStatus());
		verify(userRepository, times(0)).save(any(User.class));
	}

	private User createUser() {
		User user = new User("username", this.createUserType());
		user.setId(1);
		user.setDate(new Date());
		return user;
	}

	private UserType createUserType() {
		UserType userType = new UserType("type");
		userType.setId(1);
		return userType;
	}

}
