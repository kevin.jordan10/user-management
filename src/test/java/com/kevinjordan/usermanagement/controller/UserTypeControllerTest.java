package com.kevinjordan.usermanagement.controller;

import static com.kevinjordan.usermanagement.constant.APIConstant.DEFAULT_API;
import static com.kevinjordan.usermanagement.constant.MessageConstant.USERTYPE_DUPLICATE_NAME;
import static com.kevinjordan.usermanagement.constant.MessageConstant.USERTYPE_NOT_FOUND;
import static com.kevinjordan.usermanagement.constant.ResponseConstant.SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.kevinjordan.usermanagement.dto.UserTypeCreateDto;
import com.kevinjordan.usermanagement.util.ObjectMapperUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("local")
public class UserTypeControllerTest {

	@Autowired
	private MockMvc mockMvc;

	/**
	 * Expect the saveUserType to run successfully
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveUserTypeSuccessTest() throws Exception{
		String requestJson = ObjectMapperUtil.writeObjectAsString(createUserTypeCreateDto(null, "usertypename"));
		mockMvc.perform(post(DEFAULT_API+"/user-type")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the saveUserType to fail because the UserType which will be saved has already existed 
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveUserTypeFailureTest() throws Exception{
		String requestJson = ObjectMapperUtil.writeObjectAsString(createUserTypeCreateDto(null, "tl"));
		mockMvc.perform(post(DEFAULT_API+"/user-type")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("$.errorCode").value(USERTYPE_DUPLICATE_NAME));
	}
	
	/**
	 * Expect the updateUserType to run successfully 
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateUserTypeSuccessTest() throws Exception{
		String requestJson = ObjectMapperUtil.writeObjectAsString(createUserTypeCreateDto(1, "newusertype"));
		mockMvc.perform(put(DEFAULT_API+"/user-type")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the updateUserType to fail because the UserType which will be updated has already existed 
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateUserTypeFailureTest() throws Exception{
		String requestJson = ObjectMapperUtil.writeObjectAsString(createUserTypeCreateDto(1, "qa"));
		mockMvc.perform(put(DEFAULT_API+"/user-type")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("$.errorCode").value(USERTYPE_DUPLICATE_NAME));
	}
	
	/**
	 * Expect the deleteUserType to run successfully 
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteUserTypeSuccessTest() throws Exception {
		mockMvc.perform(delete(DEFAULT_API+"/user-type/6"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the deleteUserType to fail because the UserType which will be deleted can not be found in the repository 
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteUserTypeFailureTest() throws Exception {
		mockMvc.perform(delete(DEFAULT_API+"/user-type/20"))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorCode").value(USERTYPE_NOT_FOUND));
	}
	
	/**
	 * Expect the getUserTypeById to run successfully 
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUserTypeByIdSuccessTest() throws Exception {
		mockMvc.perform(get(DEFAULT_API+"/user-type/1"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the getUserTypeById to fail because the searched UserType can not be found in the repository 
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUserByIdTypeFailureTest() throws Exception {
		mockMvc.perform(get(DEFAULT_API+"/user-type/20"))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorCode").value(USERTYPE_NOT_FOUND));
	}
	
	/**
	 * Expect the getUserType to run successfully
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUserTypeSuccessTest() throws Exception {
		mockMvc.perform(get(DEFAULT_API+"/user-type")
				.param("name", "qa")
				.param("pageIndex", "0")
				.param("pageSize",  "5")
				.param("sortDirection", "asc")
				.param("sortTarget", "id"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}

	private UserTypeCreateDto createUserTypeCreateDto(Integer id, String name) {
		UserTypeCreateDto userTypeDto = new UserTypeCreateDto();
		if (id != null) {
			userTypeDto.setId(id);
		}
		userTypeDto.setName(name);
		return userTypeDto;
	}
}

