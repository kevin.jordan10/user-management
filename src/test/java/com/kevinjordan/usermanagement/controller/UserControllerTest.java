package com.kevinjordan.usermanagement.controller;

import static com.kevinjordan.usermanagement.constant.APIConstant.DEFAULT_API;
import static com.kevinjordan.usermanagement.constant.MessageConstant.USER_DUPLICATE_NAME;
import static com.kevinjordan.usermanagement.constant.MessageConstant.USER_NOT_FOUND;
import static com.kevinjordan.usermanagement.constant.ResponseConstant.SUCCESS;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.kevinjordan.usermanagement.dto.UserCreateDto;
import com.kevinjordan.usermanagement.util.ObjectMapperUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("local")
public class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	/**
	 * Expect the saveUser to run successfully
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveUserSuccessTest() throws Exception {
		String requestJson = ObjectMapperUtil
				.writeObjectAsString(createUserCreateDto(null, "UserName", 1));
		mockMvc.perform(post(DEFAULT_API+"/user")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the saveUser to fail because the User which will be saved has already existed 
	 * 
	 * @throws Exception
	 */
	@Test
	public void saveUserFailureTest() throws Exception  {
		String requestJson = ObjectMapperUtil
				.writeObjectAsString(createUserCreateDto(null, "JohnDoe", 1));
		mockMvc.perform(post(DEFAULT_API+"/user")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("$.errorCode").value(USER_DUPLICATE_NAME));
	}
	
	/**
	 * Expect the updateUser to run successfully 
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateUserSuccessTest() throws Exception {
		String requestJson = ObjectMapperUtil
				.writeObjectAsString(createUserCreateDto(1, "NewUserTest", 1));
		mockMvc.perform(put(DEFAULT_API+"/user")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the updateUser to fail because the User which will be updated has already existed 
	 * 
	 * @throws Exception
	 */
	@Test
	public void updateUserFailureTest() throws Exception {
		String requestJson = ObjectMapperUtil
				.writeObjectAsString(createUserCreateDto(1, "JohnDoe", 1));
		mockMvc.perform(put(DEFAULT_API+"/user")
				.content(requestJson)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isUnprocessableEntity())
				.andExpect(jsonPath("$.errorCode").value(USER_DUPLICATE_NAME));
	}
	
	/**
	 * Expect the deleteUser to run successfully 
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteUserSuccessTest() throws Exception {
		mockMvc.perform(delete(DEFAULT_API+"/user/11"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the deleteUser to fail because the User which will be deleted can not be found in the repository 
	 * 
	 * @throws Exception
	 */
	@Test
	public void deleteUserFailureTest() throws Exception {
		mockMvc.perform(delete(DEFAULT_API+"/user/20"))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorCode").value(USER_NOT_FOUND));
	}
	
	/**
	 * Expect the getUserById to run successfully 
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUserByIdSuccessTest() throws Exception {
		mockMvc.perform(get(DEFAULT_API+"/user/5"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	/**
	 * Expect the getUserById to fail because the searched User can not be found in the repository 
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUserByIdFailureTest() throws Exception {
		mockMvc.perform(get(DEFAULT_API+"/user/20"))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.errorCode").value(USER_NOT_FOUND));
	}
	
	/**
	 * Expect the getUser to run successfully
	 * 
	 * @throws Exception
	 */
	@Test
	public void getUserSuccessTest() throws Exception {
		mockMvc.perform(get(DEFAULT_API+"/user")
				.param("name", "JohnDoe")
				.param("type", "2")
				.param("pageIndex", "0")
				.param("pageSize", "5")
				.param("sortDirection", "asc")
				.param("sortTarget", "id"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.status").value(SUCCESS));
	}
	
	private UserCreateDto createUserCreateDto(Integer id, String name, Integer type) {
		UserCreateDto userDto = new UserCreateDto();
		if (id != null) {
			userDto.setId(id);
		}
		userDto.setName(name);
		userDto.setType(type);
		return userDto;
	}
	
}
