package com.kevinjordan.usermanagement.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperUtil {

	private ObjectMapperUtil() {
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(ObjectMapperUtil.class);

	private static ObjectMapper mapper = new ObjectMapper();

	public static <T> String writeObjectAsString(T object) {
		try {
			return mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			LOGGER.error(e.getLocalizedMessage());
			return null;
		}
	}

}
