package com.kevinjordan.usermanagement.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kevinjordan.usermanagement.exception.custom.InvalidDatePatternException;

public class DateUtil {

	private DateUtil() {
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtil.class);

	public static Date convertStringToDate(String date) {
		if (date == null) {
			return null;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		Date convertedDate = null;
		try {
			convertedDate = dateFormat.parse(date);
		} catch (ParseException e) {
			LOGGER.error(e.getLocalizedMessage());
			throw new InvalidDatePatternException();
		}
		return convertedDate;
	}

}
