package com.kevinjordan.usermanagement.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;

import com.kevinjordan.usermanagement.response.ErrorResponse;

@Component
public class ResponseUtil {

	@Autowired
	private MessageUtil messageUtil;

	public List<ErrorResponse> createErrorResponse(List<ObjectError> objectErrorList) {
		List<ErrorResponse> errorResponseList = new ArrayList<>();
		for (ObjectError objectError : objectErrorList) {
			errorResponseList.add(new ErrorResponse(objectError.getDefaultMessage(),
					messageUtil.toLocale(objectError.getDefaultMessage())));
		}
		return errorResponseList;
	}

	public ErrorResponse createErrorResponse(Exception e) {
		return new ErrorResponse(e.getMessage(), messageUtil.toLocale(e.getMessage()));
	}

}
