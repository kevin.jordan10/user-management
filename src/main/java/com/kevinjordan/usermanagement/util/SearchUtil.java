package com.kevinjordan.usermanagement.util;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Component;

import com.kevinjordan.usermanagement.page.Pagination;

@Component
public class SearchUtil {

	private SearchUtil() {
	}

	public static <T> Order createOrder(Pagination pagination, CriteriaBuilder criteriaBuilder, Root<T> root) {
		return pagination.getSortDirection().equals("asc") ? criteriaBuilder.asc(root.get(pagination.getSortTarget()))
				: criteriaBuilder.desc(root.get(pagination.getSortTarget()));
	}

}
