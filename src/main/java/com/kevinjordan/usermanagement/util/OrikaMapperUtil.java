package com.kevinjordan.usermanagement.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.glasnost.orika.MapperFacade;

@Component
public class OrikaMapperUtil {

	@Autowired
	private MapperFacade mapper;

	public <T1, T2> T2 map(T1 sourceObject, Class<T2> destinationClass) {
		return mapper.map(sourceObject, destinationClass);
	}

	public <T1, T2> List<T2> mapAsList(List<T1> sourceObject, Class<T2> destinationClass) {
		return mapper.mapAsList(sourceObject, destinationClass);
	}

}
