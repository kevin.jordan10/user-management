package com.kevinjordan.usermanagement.validation.group;

/**
 * Primary validation group
 * 
 * @author kevin.jordan
 */
public interface Primary {
}
