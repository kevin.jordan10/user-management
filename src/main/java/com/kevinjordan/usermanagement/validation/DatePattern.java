package com.kevinjordan.usermanagement.validation;

import static com.kevinjordan.usermanagement.constant.MessageConstant.DATE_INVALID_PATTERN;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.kevinjordan.usermanagement.validation.impl.DatePatternValidator;

@Constraint(validatedBy = DatePatternValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface DatePattern {

	String message() default DATE_INVALID_PATTERN;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
