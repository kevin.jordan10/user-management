package com.kevinjordan.usermanagement.validation;

import static com.kevinjordan.usermanagement.constant.MessageConstant.USERTYPE_NOT_EXIST;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.kevinjordan.usermanagement.validation.impl.UserTypeExistValidator;

/**
 * Annotation to check whether UserType is exist
 * 
 * @author kevin.jordan
 *
 */
@Constraint(validatedBy = UserTypeExistValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface UserTypeNotExist {

	String message() default USERTYPE_NOT_EXIST;

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
