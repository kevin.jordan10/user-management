package com.kevinjordan.usermanagement.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.kevinjordan.usermanagement.exception.custom.InvalidDatePatternException;
import com.kevinjordan.usermanagement.util.DateUtil;
import com.kevinjordan.usermanagement.validation.DatePattern;

public class DatePatternValidator implements ConstraintValidator<DatePattern, String> {

	@Override
	public boolean isValid(String date, ConstraintValidatorContext context) {
		try {
			DateUtil.convertStringToDate(date);
			return true;
		} catch (InvalidDatePatternException e) {
			return false;
		}
	}

}
