package com.kevinjordan.usermanagement.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kevinjordan.usermanagement.repository.UserRepository;
import com.kevinjordan.usermanagement.validation.UserNotExist;

/**
 * Validator for UserNotExist
 * 
 * @author kevin.jordan
 *
 */
@Component
public class UserExistValidator implements ConstraintValidator<UserNotExist, Integer> {

	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean isValid(Integer userId, ConstraintValidatorContext context) {
		return userId != null && userRepository.findById(userId).isPresent();
	}

}
