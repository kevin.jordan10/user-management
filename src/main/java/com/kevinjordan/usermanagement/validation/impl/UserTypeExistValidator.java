package com.kevinjordan.usermanagement.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kevinjordan.usermanagement.repository.UserTypeRepository;
import com.kevinjordan.usermanagement.validation.UserTypeNotExist;

/**
 * Validator for UserTypeNotExist
 * 
 * @author kevin.jordan
 *
 */
@Component
public class UserTypeExistValidator implements ConstraintValidator<UserTypeNotExist, Integer> {

	@Autowired
	private UserTypeRepository userTypeRepository;

	@Override
	public boolean isValid(Integer userTypeId, ConstraintValidatorContext context) {
		return userTypeId != null && userTypeRepository.findById(userTypeId).isPresent();
	}

}
