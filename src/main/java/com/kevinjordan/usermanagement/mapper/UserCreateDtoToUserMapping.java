package com.kevinjordan.usermanagement.mapper;

import org.springframework.stereotype.Component;

import com.kevinjordan.usermanagement.dto.UserCreateDto;
import com.kevinjordan.usermanagement.model.User;
import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.util.DateUtil;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;

@Component
public class UserCreateDtoToUserMapping implements OrikaMapperFactoryConfigurer {

	@Override
	public void configure(MapperFactory orikaMapperFactory) {
		orikaMapperFactory.classMap(UserCreateDto.class, User.class).mapNulls(true).exclude("date")
				.customize(new CustomMapper<UserCreateDto, User>() {
					@Override
					public void mapAtoB(UserCreateDto a, User b, MappingContext context) {
						b.setType(new UserType(a.getType()));
						b.setDate(DateUtil.convertStringToDate(a.getDate()));
					}
				}).byDefault().register();
	}

}
