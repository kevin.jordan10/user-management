package com.kevinjordan.usermanagement.mapper;

import org.springframework.stereotype.Component;

import com.kevinjordan.usermanagement.dto.UserSearchDto;
import com.kevinjordan.usermanagement.model.User;

import ma.glasnost.orika.CustomMapper;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;

@Component
public class UserToUserSearchDtoMapping implements OrikaMapperFactoryConfigurer {

	@Override
	public void configure(MapperFactory orikaMapperFactory) {
		orikaMapperFactory.classMap(User.class, UserSearchDto.class).mapNulls(false)
				.customize(new CustomMapper<User, UserSearchDto>() {
					@Override
					public void mapAtoB(User a, UserSearchDto b, MappingContext context) {
						b.setType(a.getType().getName());
					}
				}).byDefault().register();
	}

}
