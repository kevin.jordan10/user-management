package com.kevinjordan.usermanagement.page;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.util.StringUtils;

public class Pagination {

	private Integer pageIndex;

	private Integer pageSize;

	private String sortDirection;

	private String sortTarget;

	public Pagination(Integer pageIndex, Integer pageSize, String sortDirection, String sortTarget) {
		this.pageIndex = (pageIndex == null) ? 0 : pageIndex;
		this.pageSize = (pageSize == null) ? 5 : pageSize;
		this.sortTarget = StringUtils.isEmpty(sortTarget) ? "id" : sortTarget;
		this.sortDirection = StringUtils.isEmpty(sortDirection) ? "asc" : sortDirection;
	}

	public Pageable toPageable() {
		Direction direction = sortDirection.equalsIgnoreCase("asc") ? Direction.ASC : Direction.DESC;
		return PageRequest.of(pageIndex, pageSize, Sort.by(direction, sortTarget));
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public String getSortTarget() {
		return sortTarget;
	}

}
