package com.kevinjordan.usermanagement.page;

import java.util.List;

import org.springframework.data.domain.Page;

public class CustomPage<T> {

	private List<T> result;

	private int totalResult;

	private int resultPerPage;

	private int totalPage;

	private int currentPageIndex;

	public CustomPage(Page<T> pageObject) {
		this.totalResult = pageObject.getContent().size();
		this.resultPerPage = pageObject.getSize();
		this.currentPageIndex = pageObject.getNumber();
		this.totalPage = this.calculteTotalPage();
		this.result = this.getPartOfList(pageObject.getContent());
	}

	public List<T> getResult() {
		return result;
	}

	public int getTotalResult() {
		return totalResult;
	}

	public int getResultPerPage() {
		return resultPerPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public int getCurrentPageIndex() {
		return currentPageIndex;
	}

	private int calculteTotalPage() {
		return (int) Math.ceil((double) this.totalResult / (double) this.resultPerPage);
	}

	private List<T> getPartOfList(List<T> list) {
		int maxIndex = list.size();
		int fromIndex = (this.currentPageIndex * this.resultPerPage);
		int toIndex = (fromIndex + this.resultPerPage);
		fromIndex = (fromIndex > maxIndex) ? maxIndex : fromIndex;
		toIndex = (toIndex > maxIndex) ? maxIndex : toIndex;
		return list.subList(fromIndex, toIndex);
	}

}
