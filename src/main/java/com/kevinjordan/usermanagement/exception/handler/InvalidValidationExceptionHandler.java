package com.kevinjordan.usermanagement.exception.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.kevinjordan.usermanagement.util.ResponseUtil;

@ControllerAdvice
public class InvalidValidationExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private ResponseUtil responseUtil;

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return handleExceptionInternal(ex, responseUtil.createErrorResponse(ex.getBindingResult().getAllErrors()),
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

}
