package com.kevinjordan.usermanagement.exception.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.kevinjordan.usermanagement.exception.custom.InvalidDatePatternException;
import com.kevinjordan.usermanagement.response.DefaultResponse;
import com.kevinjordan.usermanagement.util.ResponseUtil;

@ControllerAdvice
public class InvalidDatePatternExceptionHandler {

	@Autowired
	private ResponseUtil responseUtil;

	@ExceptionHandler({ InvalidDatePatternException.class })
	public ResponseEntity<DefaultResponse> handleInvalidDatePattern(InvalidDatePatternException ex) {
		return new ResponseEntity<>(responseUtil.createErrorResponse(ex), HttpStatus.BAD_REQUEST);
	}

}
