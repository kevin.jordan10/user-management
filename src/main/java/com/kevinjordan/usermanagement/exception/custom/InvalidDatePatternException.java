package com.kevinjordan.usermanagement.exception.custom;

import static com.kevinjordan.usermanagement.constant.MessageConstant.DATE_INVALID_PATTERN;

public class InvalidDatePatternException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidDatePatternException() {
		super(DATE_INVALID_PATTERN);
	}

}
