package com.kevinjordan.usermanagement.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public abstract class DefaultResponse {

	protected String status;
	@JsonInclude(Include.NON_NULL)
	protected Object data;

	public String getStatus() {
		return status;
	}

	public Object getData() {
		return data;
	}

}
