package com.kevinjordan.usermanagement.response;

import static com.kevinjordan.usermanagement.constant.ResponseConstant.FAILURE;

public class ErrorResponse extends DefaultResponse {

	private String errorCode;
	private String errorMessage;

	public ErrorResponse(String errorCode, String errorMessage) {
		this.status = FAILURE;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
