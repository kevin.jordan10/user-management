package com.kevinjordan.usermanagement.response;

import static com.kevinjordan.usermanagement.constant.ResponseConstant.SUCCESS;

public class SuccessResponse extends DefaultResponse {

	public SuccessResponse() {
		this.status = SUCCESS;
	}

	public SuccessResponse(Object object) {
		this.status = SUCCESS;
		this.data = object;
	}

}
