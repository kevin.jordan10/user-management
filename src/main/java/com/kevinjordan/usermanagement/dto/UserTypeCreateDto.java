package com.kevinjordan.usermanagement.dto;

import static com.kevinjordan.usermanagement.constant.MessageConstant.ID_MUST_NULL;
import static com.kevinjordan.usermanagement.constant.MessageConstant.ID_NOT_NULL;
import static com.kevinjordan.usermanagement.constant.MessageConstant.NAME_LENGTH;
import static com.kevinjordan.usermanagement.constant.MessageConstant.NAME_NOT_EMPTY;
import static com.kevinjordan.usermanagement.constant.MessageConstant.NAME_NOT_NULL;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import org.hibernate.validator.constraints.Length;

import com.kevinjordan.usermanagement.validation.UserTypeNotExist;
import com.kevinjordan.usermanagement.validation.group.Insert;
import com.kevinjordan.usermanagement.validation.group.Primary;
import com.kevinjordan.usermanagement.validation.group.Update;

/**
 * Dto of UserType
 * 
 * @author kevin.jordan
 *
 */
public class UserTypeCreateDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Null(message = ID_MUST_NULL, groups = Insert.class)
	@NotNull(message = ID_NOT_NULL, groups = Update.class)
	@UserTypeNotExist(groups = { Update.class })
	private Integer id;

	@NotEmpty(message = NAME_NOT_EMPTY, groups = Insert.class)
	@NotNull(message = NAME_NOT_NULL, groups = Insert.class)
	@Length(message = NAME_LENGTH, min = 2, max = 100, groups = Primary.class)
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
