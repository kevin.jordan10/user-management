package com.kevinjordan.usermanagement.repository;

import com.kevinjordan.usermanagement.dto.UserTypeSearchDto;
import com.kevinjordan.usermanagement.page.CustomPage;
import com.kevinjordan.usermanagement.page.Pagination;

public interface UserTypeCustomRepository {

	public CustomPage<UserTypeSearchDto> findUserTypesByParameter(String name, Pagination pagination);

}
