package com.kevinjordan.usermanagement.repository;

import java.util.Date;

import com.kevinjordan.usermanagement.dto.UserSearchDto;
import com.kevinjordan.usermanagement.page.CustomPage;
import com.kevinjordan.usermanagement.page.Pagination;

public interface UserCustomRepository {

	public CustomPage<UserSearchDto> findUsersByParameter(String name, Integer type, Date startDate, Date endDate, Pagination pagination);

}
