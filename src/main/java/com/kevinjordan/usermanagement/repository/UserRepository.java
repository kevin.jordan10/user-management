package com.kevinjordan.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kevinjordan.usermanagement.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public User findUserById(Integer id);

	public List<User> findUserByName(String name);

}
