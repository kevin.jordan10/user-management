package com.kevinjordan.usermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kevinjordan.usermanagement.model.UserType;

public interface UserTypeRepository extends JpaRepository<UserType, Integer> {

	public UserType findUserTypeById(Integer id);

	public List<UserType> findUserTypeByName(String name);

}
