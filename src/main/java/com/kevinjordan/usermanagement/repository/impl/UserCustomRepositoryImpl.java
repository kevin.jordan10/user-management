package com.kevinjordan.usermanagement.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kevinjordan.usermanagement.dto.UserSearchDto;
import com.kevinjordan.usermanagement.model.User;
import com.kevinjordan.usermanagement.page.CustomPage;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.repository.UserCustomRepository;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;
import com.kevinjordan.usermanagement.util.SearchUtil;

@Repository
public class UserCustomRepositoryImpl implements UserCustomRepository {

	@Autowired
	private EntityManagerFactory entityManager;

	@Autowired
	private OrikaMapperUtil mapperUtil;

	@Override
	public CustomPage<UserSearchDto> findUsersByParameter(String name, Integer type, Date startDate, Date endDate, Pagination pagination) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteria = criteriaBuilder.createQuery(User.class);
		Root<User> root = criteria.from(User.class);
		Order order = SearchUtil.createOrder(pagination, criteriaBuilder, root);

		criteria.where(createPredicate(name, type, startDate, endDate, criteriaBuilder, root)).orderBy(order);
		List<User> users = entityManager.createEntityManager().createQuery(criteria).getResultList();
		Page<UserSearchDto> pageOfUser = new PageImpl<>(mapperUtil.mapAsList(users, UserSearchDto.class),
				pagination.toPageable(), users.size());
		return new CustomPage<>(pageOfUser);
	}

	private Predicate createPredicate(String name, Integer type, Date startDate, Date endDate, CriteriaBuilder criteriaBuilder, Root<User> root) {
		List<Predicate> predicates = new ArrayList<>();
		Predicate searchPredicateByName = null;
		Predicate searchPredicateByType = null;
		Predicate searchPredicateByDate = null;
		
		if (!StringUtils.isEmpty(name)) {
			searchPredicateByName = criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
					"%" + name.toLowerCase() + "%");
			predicates.add(searchPredicateByName);
		}
		
		if (type != null) {
			searchPredicateByType = criteriaBuilder.equal(root.get("type"), type);
			predicates.add(searchPredicateByType);
		}
		
		if (startDate != null && endDate != null) {
			searchPredicateByDate = criteriaBuilder.between(root.get("date"), startDate, endDate);
			predicates.add(searchPredicateByDate);
		}
		return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
	}

}
