package com.kevinjordan.usermanagement.repository.impl;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.kevinjordan.usermanagement.dto.UserTypeSearchDto;
import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.page.CustomPage;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.repository.UserTypeCustomRepository;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;
import com.kevinjordan.usermanagement.util.SearchUtil;

@Repository
public class UserTypeCustomRepositoryImpl implements UserTypeCustomRepository {

	@Autowired
	private EntityManagerFactory entityManager;

	@Autowired
	private OrikaMapperUtil mapperUtil;

	@Override
	public CustomPage<UserTypeSearchDto> findUserTypesByParameter(String name, Pagination pagination) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<UserType> criteria = criteriaBuilder.createQuery(UserType.class);
		Root<UserType> root = criteria.from(UserType.class);
		Order order = SearchUtil.createOrder(pagination, criteriaBuilder, root);

		criteria.where(createPredicate(name, criteriaBuilder, root)).orderBy(order);
		List<UserType> userTypes = entityManager.createEntityManager().createQuery(criteria).getResultList();
		Page<UserTypeSearchDto> pageOfUserType = new PageImpl<>(
				mapperUtil.mapAsList(userTypes, UserTypeSearchDto.class), pagination.toPageable(), userTypes.size());
		return new CustomPage<>(pageOfUserType);
	}

	private Predicate createPredicate(String name, CriteriaBuilder criteriaBuilder, Root<UserType> root) {
		return !StringUtils.isEmpty(name)
				? criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + name.toLowerCase() + "%")
				: criteriaBuilder.and();
	}

}
