package com.kevinjordan.usermanagement.constant;

public class APIConstant {

	private APIConstant() {
	}

	public static final String DEFAULT_API = "/api/v1/user-management";

}
