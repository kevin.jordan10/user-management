package com.kevinjordan.usermanagement.constant;

public class MessageConstant {

	private MessageConstant() {
	}

	public static final String USER_NOT_FOUND = "um.user.notfound";
	public static final String USER_NOT_EXIST = "um.user.notexist";
	public static final String USER_DUPLICATE_NAME = "um.user.duplicatename";

	public static final String USERTYPE_NOT_FOUND = "um.usertype.notfound";
	public static final String USERTYPE_NOT_NULL = "um.usertype.notnull";
	public static final String USERTYPE_NOT_EXIST = "um.usertype.notexist";
	public static final String USERTYPE_DUPLICATE_NAME = "um.usertype.duplicatename";

	public static final String ID_MUST_NULL = "um.id.mustnull";
	public static final String ID_NOT_NULL = "um.id.notnull";
	public static final String NAME_NOT_EMPTY = "um.name.notempty";
	public static final String NAME_NOT_NULL = "um.name.notnull";
	public static final String NAME_LENGTH = "um.name.length";
	public static final String DATE_INVALID_PATTERN = "um.date.invalidpattern";

}
