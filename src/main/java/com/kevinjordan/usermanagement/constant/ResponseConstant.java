package com.kevinjordan.usermanagement.constant;

public class ResponseConstant {

	private ResponseConstant() {
	}

	public static final String SUCCESS = "SUCCESS";
	public static final String FAILURE = "FAILURE";

}
