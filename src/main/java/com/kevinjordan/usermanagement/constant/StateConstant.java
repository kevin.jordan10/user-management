package com.kevinjordan.usermanagement.constant;

public class StateConstant {

	private StateConstant() {
	}

	public static final String UPDATE = "edit";
	public static final String INSERT = "add";

}
