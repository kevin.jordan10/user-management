package com.kevinjordan.usermanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "user_type_table")
public class UserType {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usertype_pk_generator")
	@SequenceGenerator(name = "usertype_pk_generator", initialValue = 6)
	@Column(name = "id")
	private Integer id;

	@Column(name = "name")
	private String name;

	public UserType() {
	}

	public UserType(Integer id) {
		this.id = id;
	}

	public UserType(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
