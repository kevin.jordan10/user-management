package com.kevinjordan.usermanagement.service;

import java.util.Date;

import org.springframework.http.ResponseEntity;

import com.kevinjordan.usermanagement.model.User;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.response.DefaultResponse;

public interface UserService {

	public ResponseEntity<DefaultResponse> saveUser(User user);

	public ResponseEntity<DefaultResponse> getUserById(Integer id);

	public ResponseEntity<DefaultResponse> deleteUser(Integer id);

	public ResponseEntity<DefaultResponse> updateUser(User user);

	public ResponseEntity<DefaultResponse> findUsers(String name, Integer type, Date startDate, Date endDate, Pagination pagination);

}
