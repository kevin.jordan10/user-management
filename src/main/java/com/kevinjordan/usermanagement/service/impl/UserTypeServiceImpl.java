package com.kevinjordan.usermanagement.service.impl;

import static com.kevinjordan.usermanagement.constant.MessageConstant.USERTYPE_DUPLICATE_NAME;
import static com.kevinjordan.usermanagement.constant.MessageConstant.USERTYPE_NOT_FOUND;
import static com.kevinjordan.usermanagement.constant.StateConstant.INSERT;
import static com.kevinjordan.usermanagement.constant.StateConstant.UPDATE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kevinjordan.usermanagement.dto.UserTypeSearchDto;
import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.repository.UserTypeCustomRepository;
import com.kevinjordan.usermanagement.repository.UserTypeRepository;
import com.kevinjordan.usermanagement.response.DefaultResponse;
import com.kevinjordan.usermanagement.response.ErrorResponse;
import com.kevinjordan.usermanagement.response.SuccessResponse;
import com.kevinjordan.usermanagement.service.UserTypeService;
import com.kevinjordan.usermanagement.util.MessageUtil;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;

/**
 * UserTypeService implementation
 * 
 * @author kevin.jordan
 *
 */
@Transactional
@Service
public class UserTypeServiceImpl implements UserTypeService {

	@Autowired
	private UserTypeRepository userTypeRepository;

	@Autowired
	private UserTypeCustomRepository userTypeCustomRepository;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private OrikaMapperUtil mapperUtil;

	@Override
	public ResponseEntity<DefaultResponse> saveUserType(UserType userType) {
		if (isNotDuplicateName(userType)) {
			userTypeRepository.save(userType);
			return new ResponseEntity<>(new SuccessResponse(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(
					new ErrorResponse(USERTYPE_DUPLICATE_NAME, messageUtil.toLocale(USERTYPE_DUPLICATE_NAME, INSERT)),
					HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> getUserTypeById(Integer id) {
		UserType userType = userTypeRepository.findUserTypeById(id);
		if (userType != null) {
			return new ResponseEntity<>(new SuccessResponse(mapperUtil.map(userType, UserTypeSearchDto.class)),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ErrorResponse(USERTYPE_NOT_FOUND, messageUtil.toLocale(USERTYPE_NOT_FOUND)),
					HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> deleteUserType(Integer id) {
		UserType userType = userTypeRepository.findUserTypeById(id);
		if (userType != null) {
			userTypeRepository.delete(userType);
			return new ResponseEntity<>(new SuccessResponse(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ErrorResponse(USERTYPE_NOT_FOUND, messageUtil.toLocale(USERTYPE_NOT_FOUND)),
					HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> updateUserType(UserType userType) {
		if (isNotDuplicateName(userType)) {
			userTypeRepository.save(userType);
			return new ResponseEntity<>(new SuccessResponse(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(
					new ErrorResponse(USERTYPE_DUPLICATE_NAME, messageUtil.toLocale(USERTYPE_DUPLICATE_NAME, UPDATE)),
					HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> findUserTypes(String name, Pagination pagination) {
		return new ResponseEntity<>(
				new SuccessResponse(userTypeCustomRepository.findUserTypesByParameter(name, pagination)),
				HttpStatus.OK);
	}

	private boolean isNotDuplicateName(UserType userType) {
		return userTypeRepository.findUserTypeByName(userType.getName()).isEmpty();
	}

}
