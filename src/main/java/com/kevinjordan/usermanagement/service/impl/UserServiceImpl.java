package com.kevinjordan.usermanagement.service.impl;

import static com.kevinjordan.usermanagement.constant.MessageConstant.USER_DUPLICATE_NAME;
import static com.kevinjordan.usermanagement.constant.MessageConstant.USER_NOT_FOUND;
import static com.kevinjordan.usermanagement.constant.StateConstant.INSERT;
import static com.kevinjordan.usermanagement.constant.StateConstant.UPDATE;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kevinjordan.usermanagement.dto.UserSearchDto;
import com.kevinjordan.usermanagement.model.User;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.repository.UserCustomRepository;
import com.kevinjordan.usermanagement.repository.UserRepository;
import com.kevinjordan.usermanagement.response.DefaultResponse;
import com.kevinjordan.usermanagement.response.ErrorResponse;
import com.kevinjordan.usermanagement.response.SuccessResponse;
import com.kevinjordan.usermanagement.service.UserService;
import com.kevinjordan.usermanagement.util.MessageUtil;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;

/**
 * UserService implementation
 * 
 * @author kevin.jordan
 *
 */
@Transactional
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserCustomRepository userCustomRepository;

	@Autowired
	private MessageUtil messageUtil;

	@Autowired
	private OrikaMapperUtil mapperUtil;

	@Override
	public ResponseEntity<DefaultResponse> saveUser(User user) {
		if (isNotDuplicateName(user)) {
			userRepository.save(user);
			return new ResponseEntity<>(new SuccessResponse(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(
					new ErrorResponse(USER_DUPLICATE_NAME, messageUtil.toLocale(USER_DUPLICATE_NAME, INSERT)),
					HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> getUserById(Integer id) {
		User user = userRepository.findUserById(id);
		if (user != null) {
			return new ResponseEntity<>(new SuccessResponse(mapperUtil.map(user, UserSearchDto.class)), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ErrorResponse(USER_NOT_FOUND, messageUtil.toLocale(USER_NOT_FOUND)),
					HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> deleteUser(Integer id) {
		User user = userRepository.findUserById(id);
		if (user != null) {
			userRepository.delete(user);
			return new ResponseEntity<>(new SuccessResponse(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ErrorResponse(USER_NOT_FOUND, messageUtil.toLocale(USER_NOT_FOUND)),
					HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> updateUser(User user) {
		if (isNotDuplicateName(user)) {
			if (user.getDate() == null) {
				User oldUser = userRepository.findUserById(user.getId());
				user.setDate(oldUser.getDate());
			}
			userRepository.save(user);
			return new ResponseEntity<>(new SuccessResponse(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(
					new ErrorResponse(USER_DUPLICATE_NAME, messageUtil.toLocale(USER_DUPLICATE_NAME, UPDATE)),
					HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	@Override
	public ResponseEntity<DefaultResponse> findUsers(String name, Integer type, Date startDate, Date endDate, Pagination pagination) {
		return new ResponseEntity<>(
				new SuccessResponse(userCustomRepository.findUsersByParameter(name, type, startDate, endDate, pagination)), HttpStatus.OK);
	}

	private boolean isNotDuplicateName(User user) {
		return userRepository.findUserByName(user.getName()).isEmpty();
	}

}
