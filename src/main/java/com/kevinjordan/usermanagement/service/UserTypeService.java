package com.kevinjordan.usermanagement.service;

import org.springframework.http.ResponseEntity;

import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.response.DefaultResponse;

public interface UserTypeService {

	public ResponseEntity<DefaultResponse> saveUserType(UserType userType);

	public ResponseEntity<DefaultResponse> getUserTypeById(Integer id);

	public ResponseEntity<DefaultResponse> deleteUserType(Integer id);

	public ResponseEntity<DefaultResponse> updateUserType(UserType userType);

	public ResponseEntity<DefaultResponse> findUserTypes(String name, Pagination pagination);

}
