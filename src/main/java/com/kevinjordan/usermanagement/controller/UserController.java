package com.kevinjordan.usermanagement.controller;

import static com.kevinjordan.usermanagement.constant.APIConstant.DEFAULT_API;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kevinjordan.usermanagement.dto.UserCreateDto;
import com.kevinjordan.usermanagement.model.User;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.response.DefaultResponse;
import com.kevinjordan.usermanagement.service.UserService;
import com.kevinjordan.usermanagement.util.DateUtil;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;
import com.kevinjordan.usermanagement.validation.group.Insert;
import com.kevinjordan.usermanagement.validation.group.Primary;
import com.kevinjordan.usermanagement.validation.group.Update;

@RestController
@RequestMapping(DEFAULT_API)
@Validated
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private OrikaMapperUtil mapperUtil;

	@PostMapping("/user")
	public ResponseEntity<DefaultResponse> saveUser(
			@RequestBody @Validated({ Primary.class, Insert.class }) UserCreateDto userDto) {
		return userService.saveUser(mapperUtil.map(userDto, User.class));
	}

	@GetMapping("/user/{id}")
	public ResponseEntity<DefaultResponse> getUserById(@PathVariable Integer id) {
		return userService.getUserById(id);
	}

	@GetMapping("/user")
	public ResponseEntity<DefaultResponse> getUser(
			@RequestParam(required = false) String name, 
			@RequestParam(required = false) Integer type,
			@RequestParam(required = false) String startDate,
			@RequestParam(required = false) String endDate,
			@RequestParam(required = false) Integer pageIndex, 
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String sortDirection, 
			@RequestParam(required = false) String sortTarget) {
		return userService.findUsers(name, type, DateUtil.convertStringToDate(startDate), DateUtil.convertStringToDate(endDate), new Pagination(pageIndex, pageSize, sortDirection, sortTarget));
	}

	@PutMapping("/user")
	public ResponseEntity<DefaultResponse> updateUser(
			@RequestBody @Validated({ Primary.class, Update.class }) UserCreateDto userDto) {
		return userService.updateUser(mapperUtil.map(userDto, User.class));
	}

	@DeleteMapping("/user/{id}")
	public ResponseEntity<DefaultResponse> deleteUser(@PathVariable Integer id) {
		return userService.deleteUser(id);
	}

}
