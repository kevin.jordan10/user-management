package com.kevinjordan.usermanagement.controller;

import static com.kevinjordan.usermanagement.constant.APIConstant.DEFAULT_API;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kevinjordan.usermanagement.dto.UserTypeCreateDto;
import com.kevinjordan.usermanagement.model.UserType;
import com.kevinjordan.usermanagement.page.Pagination;
import com.kevinjordan.usermanagement.response.DefaultResponse;
import com.kevinjordan.usermanagement.service.UserTypeService;
import com.kevinjordan.usermanagement.util.OrikaMapperUtil;
import com.kevinjordan.usermanagement.validation.group.Insert;
import com.kevinjordan.usermanagement.validation.group.Primary;
import com.kevinjordan.usermanagement.validation.group.Update;

@RestController
@RequestMapping(DEFAULT_API)
@Validated
public class UserTypeController {

	@Autowired
	private UserTypeService userTypeService;

	@Autowired
	private OrikaMapperUtil mapperUtil;

	@PostMapping("/user-type")
	public ResponseEntity<DefaultResponse> saveUserType(
			@RequestBody @Validated({ Primary.class, Insert.class }) UserTypeCreateDto userTypeDto) {
		return userTypeService.saveUserType(mapperUtil.map(userTypeDto, UserType.class));
	}

	@GetMapping("/user-type/{id}")
	public ResponseEntity<DefaultResponse> getUserTypeById(@PathVariable Integer id) {
		return userTypeService.getUserTypeById(id);
	}

	@GetMapping("/user-type")
	public ResponseEntity<DefaultResponse> getUserType(
			@RequestParam(required = false) String name,
			@RequestParam(required = false) Integer pageIndex, 
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String sortDirection, 
			@RequestParam(required = false) String sortTarget) {
		return userTypeService.findUserTypes(name, new Pagination(pageIndex, pageSize, sortDirection, sortTarget));
	}

	@PutMapping("/user-type")
	public ResponseEntity<DefaultResponse> updateUserType(
			@RequestBody @Validated({ Primary.class, Update.class }) UserTypeCreateDto userTypeDto) {
		return userTypeService.updateUserType(mapperUtil.map(userTypeDto, UserType.class));
	}

	@DeleteMapping("/user-type/{id}")
	public ResponseEntity<DefaultResponse> deleteUserType(@PathVariable Integer id) {
		return userTypeService.deleteUserType(id);
	}

}
