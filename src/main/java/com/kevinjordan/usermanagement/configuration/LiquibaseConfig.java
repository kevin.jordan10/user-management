package com.kevinjordan.usermanagement.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
public class LiquibaseConfig {

	/**
	 * The liquibase
	 * 
	 * @param dataSource the data source
	 * @return the spring liquibase
	 */
	@Bean
	public SpringLiquibase liquibase(DataSource dataSource) {
		SpringLiquibase liquibase = new SpringLiquibase();
		liquibase.setChangeLog("classpath:liquibase/liquibase-changeLog.xml");
		liquibase.setDataSource(dataSource);
		return liquibase;
	}

}
