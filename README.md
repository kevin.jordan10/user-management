# user-management

User Management is a Java application with basic CRUD which uses Spring Boot, JPA, Liquibase and Orikamapper.

---

### Pre-requirements:
+ Java 1.8.0_221
+ Postman v7.18.1
	+ to use the application
+ IDE (Recommeded: Eclipse 2019-09 R)
	+ to build the application (can use maven directly but the tutorial for that is not covered)
+ Postgresql 
	+ create a database with these requirements:
		+ database name: postgres
		+ username: postgres
		+ password: password
		
---

### How to clone the repository:
+ open directory where you will clone the repository
+ open a command prompt
+ execute **git clone https://gitlab.com/kevin.jordan10/user-management.git .**

---

### How to run (maven):
+ Import the repository as maven project in Eclipse, then build it using maven
	+ open Eclipse
	+ select **File >> Import >> Maven >> Existing Maven Project >> Next**
	+ open the cloned repository
	+ select the pom.xml of user-management
	+ select **Finish**
	+ wait the import and maven dependencies download to finish
	+ right click on the project folder in **Project Explorer**
	+ select **Run As >> Maven build**
	+ wait until the build has been completed
+ Run the application
	+ open the project directory and search the .jar inside **target** folder
	+ open a command prompt on the same directory where the .jar exists
	+ execute **java -jar -Dspring.profiles.active=server user-management-0.0.1-SNAPSHOT.jar**
	
---

### How to run (gradle):
+ Import the repository as maven project in Eclipse, then build it using maven
	+ open Eclipse
	+ select **File >> Import >> Gradle >> Existing Gradle Project >> Next**
	+ browse the cloned repository
	+ select the directory of cloned repository
	+ select **Finish**
	+ right click one the project folder in Project Explorer
	+ select **Gradle >> Refresh Gradle Project**
	+ open a **Gradle Task View** and select the project
	+ select **build >> build**
	+ wait until the build had been completed
+ Run the application
	+ open the project directory and search the .jar inside **build/libs** folder
	+ open a command prompt on the same directory where the .jar exists
	+ execute **java -jar -Dspring.profiles.active=server user-management-0.0.1-SNAPSHOT.jar**
	
---

### How to use:
+ Import the user_management_API.postman_collection in your postman
	+ select **File >> Import**
	+ drag the **user_management_API.postman_collection** or select it manually using **Choose File**
+ Use the APIs from the postman